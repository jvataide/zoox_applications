import pandas as pd
import numpy as np
import time
from datetime import datetime
import json
from connection import MyDB
import collections



#Grupos de dados para selecionar
dataGroups = []

BasicData = {'basic_data': ['name', 'cpf']}
# colecao = []

dataGroups.append(BasicData)
# dataGroups.append(colecao)

def isEmptyTree(iterable):
    if not (isinstance(iterable, collections.Mapping) or isinstance(iterable, list)):
        if isinstance(iterable, str):
            if(len(iterable)==0):
                return True
            else:
                return False
        else:
            return False
    else:
        if isinstance(iterable, list):
            iterable_pair = enumerate(iterable)
        else:
            iterable_pair = iterable.items()
        for key, value in iterable_pair:
            empty = isEmptyTree(iterable[key])
            if(not empty):
                return False
    return True

def saveData(data, page):
    df = pd.DataFrame(data)
    print(df)
    df.to_csv('bigdata_corp_budget' + str(page) + '.csv', sep=',', encoding='utf-8', index=False)
    print("concluiu carga", str(page), "em %s segundos." % (time.time() - start), flush=True)

def groupCheck(key):
    for key2, group in enumerate(dataGroups):
        if key in group.values():
            groupKey = group.keys()[0]
            return (True, groupKey)
    return (False, 0)

db = MyDB()

start = time.time()
print(start)

db.query("use person")
db.query("select created, providers from person where created >= 20181201 and created <= 20190110")

number = 0
page = 1
finished = False

budgetDict = {}
emptyResponsesDict = {}
data = []
count = countProblems = 0
alert = False

while not finished:
    #Pegar valores
    cursor = db._db_cur.fetchmany(120000)
    print("Concluiu busca em %s segundos." %(time.time()- start), flush=True)
    start = time.time()
    #Se não acabou
    if(cursor):
        for element in cursor:
            created = element[0]
            provider = json.loads(element[1])
            #In case data comes in wrong format
            if not isinstance(provider, dict):
                countProblems += 1
            #Check if is a bigdata_corp provider
            elif 'bigdata_corp' in provider.keys():
                groupData = []
                badGroupData = []
                providerData = provider['bigdata_corp'].items()
                for key, value in providerData:
                    #Checando se a chave faz parte de algum grupo
                    inGroup = groupCheck(key)
                    #Caso faça parte de algum grupo(olhar retorno da função)
                    if inGroup[0]:
                        groupKey = inGroup[1]
                        #Caso não seja vazia
                        if not isEmptyTree(value):
                            if not groupKey in groupData:
                                groupData.append(groupKey)
                        #Caso seja vazia
                        else:
                            if not groupKey in badGroupData:
                                badGroupData.append(groupKey)

                    #Caso não faça parte de algum grupo
                    else:
                        #Se for dado vazio
                        if(isEmptyTree(value)):
                            if (key in emptyResponsesDict.keys()):
                                emptyResponsesDict[key] += 1
                            else:
                                emptyResponsesDict[key] = 1
                        else:
                        #Se não for dado vazio
                            if(key in budgetDict.keys()):
                                budgetDict[key] += 1
                            else:
                                budgetDict[key] = 1
                #Resolver o caso dos grupos
                for groupName in badGroupData:
                    if groupName in groupData:
                        badGroupData.remove(groupName)
                    elif groupName in emptyResponsesDict.keys():
                        emptyResponsesDict[groupName] += 1
                    else:
                        emptyResponsesDict[groupName] = 1
                for groupName in groupData:
                    if groupName in budgetDict:
                        budgetDict[groupName] += 1
                    else:
                        budgetDict[groupName] = 1
                #Criando os arquivos para compilar apenas os dados do Big Data Corp
                number += 1#Número de bigdata_corp data já coletado % quantidade por página
                data.append(provider['bigdata_corp'])
                if(number==50000):
                    saveData(data, page)
                    data = []
                    page += 1

    else:
        finished = True

saveData(data, page)

# print(isEmptyTree({'a': [{'b': [], 'd': 2}], 'c':[[], {}]}))

print(time.time() - start, flush=True)
print('\n')
print(budgetDict)
print('\n')
print(emptyResponsesDict)
