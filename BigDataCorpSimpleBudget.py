import pandas as pd
import time
import json
from connection import MyDB
import tree_search_algorithms


def saveData(data, page):
    with open('bigdata_corp_budget' + str(page) + '.json', 'w') as fp:
        json.dump(data, fp)
    print("concluiu carga", str(page), "em %s segundos." % (time.time() - start), flush=True)

bigdata_corp_fields = {
                       'BasicData': False,
                       'OnlineAds': False,
                       # 'Circles': False,
                       'ProfessionData': False,
                       'Scholarship': False,
                       'Memberships': False,
                       'Domains': False,
                       'Emails': False,
                       'Adresses': False,
                       'MediaProfileAndExposure': False,
                       'FlagsAndFeatures': False,
                       'FinantialData': False,
                       'DemographicData': False,
                       'KycData': False,
                       'InterestsAndBehaviors': False,
                       'Passages': False,
                       'OnlinePresence': False,
                       'Collections': False,
                       'Processes': False,
                       'SocialAssistancePrograms': False,
                       'PersonalRelationships': False,
                       'BusinessRelationships': False,
                       'Phones': False
                       # 'vehiclesData': False
                       }


db = MyDB()

start = time.time()
print(start)

db.query("use person")
db.query("select updated, created, providers from person where updated >= 20181201 and updated <= 20190110")

number = 0
total = 0
page = 1
finished = False

pricesDict = {}
free_list = []
data = []
count = countProblems = 0
result = {}
Teste = False

while not finished:
    #Pegar valores
    cursor = db._db_cur.fetchmany(5000)
    print("Concluiu busca em %s segundos." %(time.time()- start), flush=True)
    start = time.time()
    #Se não acabou
    if(cursor):
        for element in cursor:
            total += 1
            bigdata_corp_fields_copy = bigdata_corp_fields.copy()
            updated = element[0]
            created = element[1]
            provider = json.loads(element[2])
            #In case data comes in wrong format
            if not isinstance(provider, dict):
                countProblems += 1
            #Check if is a bigdata_corp provider
            elif 'bigdata_corp' in provider.keys():
                number += 1

                groupData = []
                badGroupData = []
                providerData = provider['bigdata_corp']

                data_found = tree_search_algorithms.dfs_multiple_outputs(providerData, 'SourceName')
                data_found = list(set(data_found))
                for key in data_found:
                    if key in result:
                        result[key] += 1
                    else:
                        result[key] = 1

                data.append(providerData)

    else:
        finished = True

saveData(data, page)


print(time.time() - start, flush=True)
print('\n')


result['bigdata_corp'] = number
result['total'] = total
with open('data_updated.json', 'w') as fp:
    json.dump(result, fp)


print(number)
print(total)