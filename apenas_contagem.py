import time
import json
from connection import MyDB
import pandas as pd
# import matplotlib.pyplot as plt

initialMonth = 7
intervalNumber = 6
intervalLength = 1
initialYear = 2018

results = []
all_results = []
reports = []

for i in range(intervalNumber):
    db = MyDB()

    start = time.time()
    print(start)


    startMonth = (initialMonth + i - 1)%12 + 1
    endMonth = (startMonth + intervalLength - 1)%12 + 1

    startYear = initialYear + (initialMonth + i -1)//12
    endYear = initialYear + (initialMonth + i + intervalLength -1)//12

    startMonth = str(startMonth)
    endMonth = str(endMonth)
    startYear = str(startYear)
    endYear = str(endYear)

    if int(startMonth) < 10:
        startMonth = "0" + startMonth
    if int(endMonth) < 10:
        endMonth = "0" + endMonth

    db.query("use person")
    db.query("select created, updated, providers from person where updated >= " + startYear + startMonth + "01 and updated < " + endYear + endMonth + "01")

    finished = False

    count = 0
    alert = False
    counterDir = {}
    countProblems = 0
    dif_dates_count = 0
    while not finished:
        #Pegar valores
        cursor = db._db_cur.fetchmany(100000)
        print("Concluiu busca em %s segundos." %(time.time()- start), flush=True)
        print("Até agora foram:", count)
        start = time.time()
        #Se não acabou
        if(cursor):
            for element in cursor:
                created = element[0]
                updated = element[1]
                if created != updated:
                    dif_dates_count += 1
                provider = json.loads(element[2])
                if not isinstance(provider, dict):
                    print("Date:", created, "Problem:", provider)
                    countProblems += 1
                else:
                    for key in provider.keys():
                        if key in counterDir:
                            counterDir[key] += 1
                        else:
                            counterDir[key] = 1
                    count += 1
        else:
            finished = True

    print(time.time() - start, flush=True)
    print(counterDir)
    print(count)
    if(not 'bigdata_corp' in counterDir):
        counterDir['bigdata_corp'] = 0

    results.append((counterDir['bigdata_corp'], count, countProblems, dif_dates_count))
    all_results.append(counterDir)
    reports.append("Ano " + startYear + " mês " + startMonth + " até ano " + endYear + " mês " + endMonth + " teve " + str(counterDir['bigdata_corp']) + " registros do bigdata_corp e " + str(count) + " registros totais.\nTambém houveram " + str(countProblems) + " erros.")

for report in reports:
    print(report)

df = pd.DataFrame(results, columns=['Big Data Corp', 'Total', 'Problems', 'Diff Dates'])
print(df)
df.to_csv('Results.csv', index=False)