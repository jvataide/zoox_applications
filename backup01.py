import pandas as pd

# Quais são os grupos melhor avaliados?
# Qual porcentagem receberam nota maxima 10?
# Qual a média de horário têm mais avaliação com nota 10?
# Avaliação por segmento

banList = """zoox
test"""
banList = banList.split('\n')
banList2 = """de
da
na"""
banList2 = banList2.split('\n')
groupList = """rodoviária
novo
rio"""
groupList = groupList.split('\n')

def createWordGroup(string):
    string = string.lower()
    nameList = string.split(' ')
    for name in nameList:
        if(name not in groupList):
            groupList.append(name)
    return False

def isBannedName(string):
    print(string)
    string = string.lower()
    for company in banList:
        if(company.lower() in string):
            return True
    return False

df = pd.read_csv('nps_table.csv')

empresas = pd.read_csv('ranking_empresas_mais_avaliadas.csv')
#empresas = empresas[~empresas['company_name'].transform(isBannedName)]
#empresas = empresas.reset_index(drop=True)
#empresas.to_csv('ranking_empresas_mais_avaliadas.csv', index=False)

empresas2 = empresas.copy()
empresas2.__delitem__('count_answer')
empresas2.to_csv('company_list.csv', index=False)

empresas2['company_name'].transform(createWordGroup)
print(groupList)

goodGradeN = df[df['answer']>=9].groupby('answer')['answer'].count().sum()
badGradeN = df[df['answer']<=6].groupby('answer')['answer'].count().sum()
maxGradeN = df[df['answer']==10].groupby('answer')['answer'].count().sum()
gradeN = df.shape[0]

print('Good grades:', goodGradeN/gradeN*100, '%')
print('Bad grades:', badGradeN/gradeN*100, '%')
print('Max grades:', maxGradeN/gradeN*100, '%')
wordScore = []
for word in groupList:
    wordScore.append(0)

for value in empresas2.values:
    for name in value:
        name = name.lower()
        nameList = name.split(' ')
        for name in nameList:
            for key, element in enumerate(groupList):
                if(element==name):
                    wordScore[key] += 1
                    break


wordPair = list(zip(wordScore, groupList))
wordPair= sorted(wordPair, reverse=True)
facilitate = []
for el in wordPair:
    if(el[0]==1):
        break
    facilitate.append(el)
print(facilitate)
with open("groupList.txt", "w") as text_file:
    for score, name in facilitate:
        print('Score:', score,'\tName:',name,'\n')


