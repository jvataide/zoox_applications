import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# Qual porcentagem receberam nota maxima 10?    Feito
# Qual a média de horário têm mais avaliação com nota 10? Amanda fez
# Quais são os grupos melhor avaliados?    Feito
# Avaliação por segmento    Feito

banList = """zoox
test
demo"""
banList = banList.split('\n')

banList2 = """bodytech
formula
vezpa
master"""
banList2 = banList2.split('\n')


def isBannedName(string):
    #print(string)
    string = string.lower()
    for company in banList:
        if(company.lower() in string):
            return True
    return False

def timeGroups(time):
    time = pd.to_datetime(time).hour

    step = 1
    start_time = 0
    end_time = start_time + step

    for group in range(int(24/step)):
        if(start_time<=time<end_time):
            return  (group, "Time interval "+str(start_time)+" - "+str(end_time)+".")
        start_time = end_time
        end_time += step

def isMaster(series):
    array = []
    for element in series.values:
        for element2 in wordList:
            if element2 in element.lower():
                valid = True
                for element3 in banList2:
                    if(element3 in element.lower()):
                        valid = False
                        break
                if valid:
                        array.append(element)
    return pd.DataFrame(array, columns=['coluna']).groupby('coluna')

df = pd.read_csv('nps_table.csv')
companies_df = pd.read_csv('company_list.csv')
prep_df = companies_df.set_index('company_name', drop=True)
prep_df = prep_df.sort_index()
df = df[["created", "duration", "person_id", "person_name","answer", "company_name", "company_id", "hotspot_name"]]
#group1 = df['company_name'].transform(isBannedName)
#df.to_csv('nps_table.csv')
df = df[~df['company_name'].transform(isBannedName)]
df = df.reset_index(drop=True)
df = pd.read_csv('nps_with_steroids.csv')
goodGradeN = df[df['answer']>=9].groupby('answer')['answer'].count().sum()
badGradeN = df[df['answer']<=6].groupby('answer')['answer'].count().sum()
maxGradeN = df[df['answer']==10].groupby('answer')['answer'].count().sum()
gradeN = df.shape[0]
print('Good grades:', goodGradeN/gradeN*100, '%')
print('Bad grades:', badGradeN/gradeN*100, '%')
print('Max grades:', maxGradeN/gradeN*100, '%')

# df['group'] = df['company_name'].transform(lambda x: prep_df.loc[x]['group'] if x in prep_df.index else np.nan)
# df['classification'] = df['company_name'].transform(lambda x: prep_df.loc[x]['classification'] if x in prep_df.index else np.nan)

df = df.set_index(df['created'])
grouped = df.groupby(timeGroups)
meanByHour = grouped['answer'].mean()
meanGraphPrep = meanByHour.reset_index(drop=True)
print(meanGraphPrep)

plt.plot(meanGraphPrep)
plt.show()
classifications = df.groupby('classification')['answer']
classifications_mean = classifications.mean()
transp = classifications_mean.transpose()
plt.barh(y = classifications_mean.index, width=classifications_mean.values)
plt.show()
#print(classifications_mean)