import mysql

class Connection:
	
	def __init__(self, host, username, password, database):

		self.host = host
		self.username = username
		self.password = password
		self.database = database
		self._db_cursor = None


	def make_connection(self):
		return mysql.connector.connect(host=self.host, 
									   user=self.username,
									   passwd=self.password,
									   database=self.database)

	def make_cursor(self):
		self._db_cursor = self.make_connection()


	def query(self, query):
		return self._db_cursor.execute(query, None)

	def close(self):
		return self._db_cursor.close()
