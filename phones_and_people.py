import pandas as pd
import numpy as np
import time
from datetime import datetime
import json
from connection import MyDB
import tree_search_algorithms as tsa

#Importanto as buscas em árvore
dfs_find_property = tsa.dfs_find_property
bfs_find_property = tsa.bsf_find_property

#Declarando parametros
important_provider_list = ['zooxwifi', 'bigdata_corp', 'fnrh']
v = important_provider_list
cpf_order = [v[0], v[1],v[2]]
phone_order = [v[2], v[1], v[0]]
order_dict = {'cpf': cpf_order, 'phone': phone_order, 'telefone': phone_order}

test_df = pd.DataFrame()

def saveData(data, page):
    df = pd.DataFrame(data)



    print(df)
    df.to_csv('phones_data' + str(page) + '.csv', sep=',', encoding='utf-8', index=False)
    print("concluiu carga", str(page), "em %s segundos." % (time.time() - start), flush=True)
#Inicializando DB
db = MyDB()

start = time.time()
print(start)

db.query("use person")
db.query("select providers from person")

finished = False
data = []
count = countProblems = 0
alert = False
count = 1
while count<= 5:
    #Pegar valores
    print('Primeira busca')
    cursor = db._db_cur.fetchmany(120000)
    print("Concluiu busca em %s segundos." %(time.time()- start), flush=True)
    start = time.time()
    #Se não acabou
    if(cursor):
        for element in cursor:
            #Onde ficará guardada toda a informação desejada do elemento
            element_data = {}
            providers = json.loads(element[0])
            #Procurando cada propriedade
            for property, providers_order in order_dict.items():
                #Valor padrão para caso não ache
                element_data[property] = False
                for provider in providers_order:
                    #Se o provider estiver na lista de prioridade
                    if provider in providers:
                        found_property = bfs_find_property(providers[provider], property)
                        if found_property:
                            element_data[property] = found_property
                            break
                for provider in providers:
                    #Se o provider não estiver na lista de prioridade
                    if not provider in providers_order:
                        found_property = bfs_find_property(providers[provider], property)
                        if(found_property):
                            element_data[property] = found_property
                            break
            #For testing
            if element_data['telefone']:
                element_data['phone'] = element_data['telefone']
            element_data.pop('telefone', None)
            data.append(element_data)

        saveData(data, count)
        count += 1
    else:
        finished = True


print(time.time() - start, flush=True)
