def dfs_find_property(value, property):
    if isinstance(value, dict):
        if property in value.keys():
            return value[property]
        else:
            for key, val in value.items():
                found_property = dfs_find_property(val, property)
                if found_property:
                    return found_property
    elif isinstance(value, list):
        for val in value:
            found_property = dfs_find_property(val, property)
            if found_property:
                return found_property
    return False

def bsf_find_property(value, property):
    search_list = [value]
    while search_list:
        node = search_list.pop(0)
        if isinstance(node, list):
            search_list.extend(node)
        elif isinstance(node, dict):
            if property in node.keys():
                return node[property]
            for edge in node:
                search_list.append(node[edge])
    return False

def dfs_multiple_outputs(value, property):
    found_properties = []
    if isinstance(value, dict):
        if property in value.keys():
            found_properties.append(value[property])
        else:
            for key, val in value.items():
                found_property = dfs_multiple_outputs(val, property)
                found_properties.extend(found_property)
    elif isinstance(value, list):
        for val in value:
            found_property = dfs_multiple_outputs(val, property)
            found_properties.extend(found_property)
    return found_properties